// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;

import java.io.FileInputStream;
import java.io.PrintStream;
import java.net.URI;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.Properties;
import net.minecraft.client.Minecraft;
import org.lwjgl.opengl.GL11;

// Referenced classes of package net.minecraft.src:
//            BaseMod, ModLoader, KeyBinding, ScaledResolution, 
//            GameSettings, RenderEngine, EntityPlayerSP, GuiIngame, 
//            InventoryPlayer

public class mod_DualWielding extends BaseMod
{
    public String getVersion() { return "V1.1"; }

    public static boolean DUAL_WIELDING_ENABLED = true;
    
    //public static mod_DualWielding instance;
    public static KeyBinding keyBindToggleDW;
    public static KeyBinding keyBindUse = new KeyBinding("LHandActive", 29);
    
    
    public mod_DualWielding()
    {
        ModLoader.setInGameHook(this, true, false);
        ModLoader.setInGUIHook(this, true, false);
        keyBindToggleDW = new KeyBinding("Toggle DW", 21);
        ModLoader.registerKey(this, keyBindUse, false);
        ModLoader.registerKey(this, keyBindToggleDW, false);
    }

    public void load()
    {
    }


    public boolean onTickInGame(float f, Minecraft mc)
    {
        ScaledResolution scaledresolution = new ScaledResolution(mc.gameSettings, mc.displayWidth, mc.displayHeight);
        int i = scaledresolution.getScaledWidth();
        int j = scaledresolution.getScaledHeight();
        if(!mc.gameSettings.hideGUI || mc.currentScreen != null)
        {
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, mc.renderEngine.getTexture("/DualWield/LHInd.png"));
            //float f = mc.ingameGUI.zLevel;
            mc.ingameGUI.zLevel = -90F;
            if(DUAL_WIELDING_ENABLED)
            {
            	mc.ingameGUI.drawTexturedModalRect((i / 2 - 91 - 1) + mc.thePlayer.inventory.currentItemLeft * 20, j - 22 - 1, 0, 0, 24, 24);
            }
            mc.ingameGUI.zLevel = f;
        }
        return true;
    }

    public boolean onTickInGUI(float f, Minecraft mc, GuiScreen var3)
    {
        return onTickInGame(f, mc);
    }

    private static void LoadProperties()
    {
        Properties properties = new Properties();
        try
        {
            String s = (net.minecraft.src.mod_DualWielding.class).getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
            s = s.substring(0, s.lastIndexOf('/') + 1);
            s = (new StringBuilder()).append(s).append("mod_DualWielding_1.1_for_1.8.1").toString();
            properties.load(new FileInputStream(s));
        }
        catch(Exception exception)
        {
            System.out.println("Error loading properties.");
            exception.printStackTrace();
        }
    }

    static 
    {
        LoadProperties();
    }
}
